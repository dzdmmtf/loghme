package com.loghme.controllers;

import com.loghme.database.OrderMapper;
import com.loghme.database.UserMapper;
import com.loghme.repository.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class OrderServlet {
    @RequestMapping(value = "/orders", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object finalizeOrder() throws ParseException, InterruptedException {
        JSONObject currentOrder = Cart.getJsonCart();
        if ((Integer) currentOrder.get("totalPrice") > User.getCredit()) {
            String statusCode = "FORBIDDEN";
            JSONObject statusCodeJson = new JSONObject();
            statusCodeJson.put("statusCode", statusCode);
            return statusCodeJson.toString();
        } else {
            int updatedCredit = User.getCredit() - (Integer) currentOrder.get("totalPrice");
            User.updateUserCredit(User.getEmail(), updatedCredit);
            User.setCredit(updatedCredit);
            return new ResponseEntity<>(HttpStatus.OK);
        }
    }

    @RequestMapping(value= "/orders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String  getTotalOrders() throws ParseException{
        JSONObject totalOrders = new JSONObject();
        JSONObject newOrder;
        if(Delivery.getStatus().equals("found"))
            newOrder = Order.getOrdersFromDBBaseOnStatus(User.getEmail(), "found");
        else
            newOrder = Order.getOrdersFromDBBaseOnStatus(User.getEmail(), "finding");
        newOrder.put("status", Delivery.getStatus());
        totalOrders.put("oldOrders", Cart.getOlderUsersOrder());
        totalOrders.put("currentOrders", newOrder);
        return totalOrders.toString();
    }
}
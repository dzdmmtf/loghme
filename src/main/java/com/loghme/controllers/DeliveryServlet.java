package com.loghme.controllers;

import com.loghme.database.OrderMapper;
import com.loghme.repository.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DeliveryServlet {
    public static int DURATION = 0;
    public long activatedAt = Long.MAX_VALUE;
    public void activate() {
        activatedAt = System.currentTimeMillis();
    }

    public boolean isActive() {
        long activeFor = System.currentTimeMillis() - activatedAt;

        return activeFor >= 0 && activeFor <= DURATION;
    }

    @RequestMapping(value = "/delivery", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void getDelivery() {
        try {
            JSONObject restaurantLocation = new JSONObject();
            restaurantLocation.put("x", 00000l);
            restaurantLocation.put("y", 00000l);
            String deliveryId = Delivery.calculateTimeOfDeliveries(restaurantLocation);
            JSONObject deliveryInfo = Delivery.getDeliveryInfo();
            System.out.println("DELIVERY INFO :: "+ deliveryInfo);
//            deliveryId = "1";
            if(deliveryId.equals("0")){
                Delivery.setStatus("searching");
            }
            else{
                Long duration = (Long)deliveryInfo.get("arriving time");
                DURATION = (duration.intValue())*1000;
//                DURATION = (15)*1000;
                this.activate();
                boolean temp = false;
                while (isActive()){
                    Delivery.setStatus("found");
                    if(!temp) {
                        Order.changeFindingStatusToFoundStatus(User.getEmail());
                        temp = true;
                    }
                }
                Order.changeFoundStatusToDeliveredStatus(User.getEmail());
                User.setOrderId((int)(Math.random()*((10000)+1)));
                Delivery.setStatus("searching");
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
package com.loghme.controllers;

import com.loghme.repository.FoodParty;
import com.loghme.repository.Order;
import com.loghme.repository.Restaurant;
import com.loghme.repository.User;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;
import java.io.*;
import javax.annotation.PostConstruct;

@Component
public class LoghmeServlet {
    @PostConstruct
    public void init() {
        try {
            System.out.println("----START------");
            Restaurant.externalServerConnection();
            FoodParty.externalServerConnection();
            User.createUserTable();
            Order.createUserOrderTable();
        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }
    }
}

package com.loghme.controllers;
import com.loghme.repository.User;
import org.json.simple.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;


@RestController
public class ProfileServlet {

    @RequestMapping(value = "/profile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getProfile(){
        JSONObject profile = new JSONObject();
        profile.put("fullname", User.getName() + ' ' + User.getLastName());
        profile.put("phone", User.getPhone());
        profile.put("email", User.getEmail());
        profile.put("credit", User.getCredit());
        return profile.toString();
    }
}
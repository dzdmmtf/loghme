package com.loghme.controllers;

import com.loghme.repository.Cart;
import com.loghme.repository.Order;
import com.loghme.repository.Restaurant;
import com.loghme.repository.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.TimeUnit;


@RestController
public class CartServlet {
    @ResponseBody
    @RequestMapping(value = "/cart", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getCart() throws ParseException, InterruptedException {
        TimeUnit.SECONDS.sleep(1);
        System.out.println("CART :: " + Cart.getJsonCart());
        return Cart.getJsonCart().toString();
    }

    @RequestMapping(value = "/cart/num", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getAmountItems() {
        int sum = Cart.getOrdersNum();
        JSONObject sumObject = new JSONObject();
        sumObject.put("amount", sum);
        return sumObject.toString();
    }

    @RequestMapping(value = "/cart", method = RequestMethod.POST)
    public Object addToCart (
            @RequestParam(value = "foodId") String foodId,
            @RequestParam(value = "restaurantId") String restaurantId,
            @RequestParam(value = "orderAmount") String orderAmount) throws ParseException {
        String foodName = Restaurant.getFoodNameById(foodId);
        JSONObject statusCodeJson = new JSONObject();
        String statusCode;
        if (Cart.validCart(restaurantId)) {
            if (Restaurant.validRestaurantAndFood(restaurantId, foodName)) {
                for (int i = 0; i < Integer.parseInt(orderAmount); i++) {
                    String restaurantName = Restaurant.getRestaurantName(restaurantId);
                    Order.addNewItemToUserOrderDB(restaurantName, restaurantId, foodName, User.getEmail(), User.getOrderId());
                    System.out.println("Your order registered successfully");
                }
                statusCode = "OK";
            } else {
                System.out.println("Invalid food or restaurant");
                statusCode = "NOT_FOUND";
            }
            statusCodeJson.put("statusCode", statusCode);
            return statusCodeJson.toString();
        } else {
            System.out.println("You have order from another restaurant");
            statusCode = "FORBIDDEN";
            statusCodeJson.put("statusCode", statusCode);
            return statusCodeJson.toString();
        }
    }

    @RequestMapping(value = "/cart/single", method = RequestMethod.POST)
    public Object incOrder(
            @RequestParam(value = "orderedFoodId") String orderedFoodId,
            @RequestParam(value = "restaurantId") String restaurantId
    ) throws ParseException, InterruptedException {
        String foodName = "";
        JSONObject cart = Cart.getJsonCart();
        JSONArray orderedFoods = (JSONArray) cart.get("order");
        JSONObject jsonObj;
        for (int i = 0; i < orderedFoods.size(); i++) {
            JSONParser parser = new JSONParser();
            jsonObj = (JSONObject) parser.parse(orderedFoods.get(i).toString());
            String id = (String) jsonObj.get("orderedFoodId");
            if (id.equals(orderedFoodId)) {
                foodName = (String) jsonObj.get("name");
                String restaurantName = Restaurant.getRestaurantName(restaurantId);
                Order.addNewItemToUserOrderDB(restaurantName, restaurantId, foodName, User.getEmail(), User.getOrderId());
                break;
            }
        }
        return Cart.getJsonCart().toString();
    }

    @ResponseBody
    @RequestMapping(value = "/cart", method = RequestMethod.DELETE)
    public Object decOrder(@RequestBody String body) throws ParseException, InterruptedException {
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        String foodName = "";
        JSONObject cart = Cart.getJsonCart();
        JSONArray orderedFoods = (JSONArray) cart.get("order");
        JSONObject jsonObj;
        for (Object orderedFood : orderedFoods) {
            JSONParser parser1 = new JSONParser();
            jsonObj = (JSONObject) parser1.parse(orderedFood.toString());
            String id = (String) jsonObj.get("orderedFoodId");
            if (id.equals(reqBody.get("orderedFoodId"))) {
                foodName = (String) jsonObj.get("name");
                String restaurantId = (String) cart.get("restaurantId");
                Order.decOrDelItemFromUserOrderDB(restaurantId, foodName, User.getEmail());
                break;
            }
        }
        return Cart.getJsonCart().toString();
    }
}
package com.loghme.controllers;

import com.loghme.repository.Restaurant;
import com.loghme.repository.User;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class RestaurantsServlet {
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value="/restaurants")
    public String getRestaurants(@RequestBody String body) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        return Restaurant.getRestaurants(reqBody.get("page").toString(), reqBody.get("count").toString()).toString();
    }

    @ResponseBody
    @RequestMapping(value = "/restaurants/{restaurantId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getRestaurant(@PathVariable String restaurantId) throws InterruptedException, ParseException {
        return Restaurant.getRestaurant(restaurantId).toString();
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value="/restaurants/search")
    public String searchRestaurant(@RequestBody String body) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        String name = reqBody.get("name").toString();
        JSONObject jsonObject = Restaurant.searchByName(name);
        JSONArray jsonArray = new JSONArray();
        jsonArray.add(jsonObject);
        return jsonArray.toString();
    }

    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value="/food/search")
    public String searchFood(@RequestBody String body) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        String name = reqBody.get("name").toString();
        JSONArray jsonArray = Restaurant.searchByFood(name);
        return jsonArray.toString();
    }
}
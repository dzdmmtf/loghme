package com.loghme.controllers;

import com.loghme.database.UserMapper;
import com.loghme.repository.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;


@RestController
public class AccountServlet {
    @ResponseStatus(value = HttpStatus.OK)
    @PostMapping(value="/account")
    public void chargeAccount(@RequestBody String body) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        int updatedCredit = User.getCredit() + Integer.parseInt((String) reqBody.get("credit"));
        User.updateUserCredit(User.getEmail(), updatedCredit);
        User.setCredit(updatedCredit);
    }
}
package com.loghme.controllers;

import com.loghme.repository.*;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class FoodPartyServlet {
    @RequestMapping(value = "/foodParty", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getFoodParty(){
        return FoodParty.getFoodPartyList().toString();
    }

    @RequestMapping(value = "/foodParty", method = RequestMethod.POST)
    public Object addToCart(
            @RequestParam(value = "orderAmount") String orderAmount,
            @RequestParam(value = "restaurantId") String restaurantId)
            throws ParseException {
        String statusCode;
        JSONObject statusCodeJson = new JSONObject();
        boolean isFinish = false;
        boolean isValidAmount = false;
        String foodName = FoodParty.getFoodName(restaurantId);
        if(!Restaurant.restaurantExists(restaurantId))
            FoodParty.addFoodPartRestToRests(restaurantId);
        else
            FoodParty.updateRestaurantMenu(restaurantId);
        if(FoodParty.getFoodCount(restaurantId, foodName).equals("0")) {
            System.out.println("Sorry this food is finished!");
            isFinish = true;
        }
        else {
            isValidAmount = FoodParty.validateOrderedAmount(restaurantId, foodName, orderAmount);
            if(isValidAmount)
                FoodParty.updateFoodCount(restaurantId, foodName, true, orderAmount);
        }
        if(!isFinish && isValidAmount) {
            if (Cart.validCart(restaurantId)) {
                if (Restaurant.validRestaurantAndFood(restaurantId, foodName)) {
                    for(int i=0;i<Integer.parseInt(orderAmount);i++) {
                        String restaurantName = Restaurant.getRestaurantName(restaurantId);
                        Order.addNewItemToUserOrderDB(restaurantName, restaurantId, foodName, User.getEmail(), User.getOrderId());
                        System.out.println("FOOD PARTY::Your order registered successfully");
                    }
                    statusCode = "OK";
                    statusCodeJson.put("statusCode", statusCode);
                    return statusCodeJson.toString();
                }
            } else {
                System.out.println("FOOD PARTY::You have order from another restaurant");
                statusCode = "FORBIDDEN";
                statusCodeJson.put("statusCode", statusCode);
                return statusCodeJson.toString();
            }
        }
        statusCode = "FOOD PARTY::FORBIDDEN-FINISHED";
        statusCodeJson.put("statusCode", statusCode);
        return statusCodeJson.toString();
    }
}

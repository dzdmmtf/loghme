package com.loghme.controllers;

import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.loghme.database.UserMapper;

import java.time.LocalDateTime;

import io.jsonwebtoken.ExpiredJwtException;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.Map;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class JWTFilter implements Filter {

    public JWTFilter() {
    }


    public void destroy() {
    }


    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        final String requestTokenHeader = request.getHeader("Authorization");
        String jwtToken;
        System.out.println("TOKEN :: " + requestTokenHeader);
        if (request.getServletPath().equals("/jwtChecker")) {
            if (!requestTokenHeader.startsWith("Bearer null")) {
                resp.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            jwtToken = requestTokenHeader.substring(7);
            DecodedJWT jwt = JWT.decode(jwtToken);
            if (jwt.getExpiresAt().compareTo(new Date()) < 0) {
                resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }
        }

        if (!request.getServletPath().equals("/login") && !request.getServletPath().equals("/signup")
                && !request.getServletPath().equals("/jwtChecker") && !request.getServletPath().equals("/login/google")) {
            if (requestTokenHeader == null) {
                resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                return;
            } else if (!requestTokenHeader.startsWith("Bearer ")) {
                resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            } else {
                jwtToken = requestTokenHeader.substring(7);
                try {
                    if (requestTokenHeader.startsWith("Bearer null")) {
                        resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
                        return;
                    }
                    DecodedJWT jwt = JWT.decode(jwtToken);
                    Map<String, Claim> claims = jwt.getClaims();
                    if (!UserMapper.checkUserExistence(claims.get("userId").asString()) || jwt.getExpiresAt().compareTo(new Date()) < 0) {
                        resp.setStatus(HttpServletResponse.SC_FORBIDDEN);
                        return;
                    }

                    resp.setStatus(HttpServletResponse.SC_OK);
                } catch (IllegalArgumentException e) {
                    System.out.println("Unable to get JWT Token");
                }
            }
        }

        chain.doFilter(request, servletResponse);
    }

    public void init(FilterConfig fConfig) throws ServletException {
    }
}
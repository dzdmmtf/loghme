package com.loghme.controllers;

import com.loghme.repository.GoogleAuthentication;
import com.loghme.repository.JsonWebToken;
import com.loghme.repository.MD5;
import com.loghme.repository.User;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;

@RestController
public class UserServlet {
    public UserServlet() {
    }

    @PostMapping(value = "/signup")
    public Object signUp(@RequestBody String body) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        String email = (String) reqBody.get("email");
        String password = (String) reqBody.get("password");
        String name = (String) reqBody.get("name");
        String familyName = (String) reqBody.get("familyName");
        String phone = (String) reqBody.get("phone");

        JSONObject status = new JSONObject();
        if (!User.checkUserExistence(email)) {
            User.insertUser(name, familyName, email, phone, MD5.getMd5(password));
            User.setName(name);
            User.setLastName(familyName);
            User.setEmail(email);
            User.setPhone(phone);
            User.setCredit(0);
            User.setOrderId((int) (Math.random() * ((10000) + 1)));
            User.setSessionId((int) (Math.random() * ((10000) + 10)));
            String jwt = JsonWebToken.createToken(email);
            status.put("status", "OK");
            status.put("jwt", jwt);
        } else {
            status.put("status", "FORBIDDEN");
            status.put("jwt", "");
        }
        return status.toString();
    }

    @PostMapping(value = "/login")
    public Object login(@RequestBody String body) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        String email = (String) reqBody.get("email");
        String password = (String) reqBody.get("password");

        JSONObject status = new JSONObject();
        if (User.checkUserExistence(email)) {
            if (User.authenticationByPass(email, MD5.getMd5(password))) {
                JSONObject userInfo = User.retrieveUserInfoByEmail(email);
                User.setUser(userInfo);
                String jwt = JsonWebToken.createToken(User.getEmail());
                status.put("status", "OK");
                status.put("jwt", jwt);
            } else {
                status.put("status", "FORBIDDEN");
                status.put("jwt", "");
            }
        } else {
            status.put("status", "NOT_FOUND");
            status.put("jwt", "");
        }
        return status.toString();
    }

    @PostMapping(value = "/login/google")
    public String googleLogin(@RequestBody String body) throws Exception {
        JSONObject status = new JSONObject();
        JSONParser parser = new JSONParser();
        JSONObject reqBody = (JSONObject) parser.parse(body);
        String idTokenString = (String) reqBody.get("idToken");

        Payload payload = GoogleAuthentication.check(idTokenString);
        String email = payload.getEmail();

        if (User.checkUserExistence(email)) {
            JSONObject userInfo = User.retrieveUserInfoByEmail(email);
            User.setUser(userInfo);
            String jwt = JsonWebToken.createToken(User.getEmail());
            status.put("status", "OK");
            status.put("jwt", jwt);
        } else {
            status.put("status", "NOT_FOUND");
            status.put("jwt", "");
        }
        return status.toString();
    }

    @RequestMapping(value = "/jwtChecker", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object jwtCheckerForLoginAndSignUp() {
        return new JSONObject().toString();
    }

    @ResponseBody
    @RequestMapping(value = "/logout", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Object logout() {
        User.setName(null);
        User.setLastName(null);
        User.setEmail("");
        User.setPhone("");
        User.setCredit(0);
        return new JSONObject();
    }
}
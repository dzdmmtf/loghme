package com.loghme.repository;

import com.loghme.database.OrderMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.util.List;

public class Order {
    public static void createUserOrderTable(){
        OrderMapper.createTable();
    }

    public static void addNewItemToUserOrderDB(String restaurantName, String restaurantId, String foodName, String email, int orderId){
        OrderMapper.addNewItemToUserOrderDB(restaurantName, restaurantId, foodName, email, orderId);
    }

    public static void decOrDelItemFromUserOrderDB(String restaurantId, String foodName, String email){
        OrderMapper.decOrDelItemFromUserOrderDB(restaurantId, foodName, email);
    }

    public static void changeFindingStatusToFoundStatus(String email){
        OrderMapper.changeFindingStatusToFoundStatus(email);
    }

    public static void changeFoundStatusToDeliveredStatus(String email){
        OrderMapper.changeFoundStatusToDeliveredStatus(email);
    }

    public static JSONObject getOrdersFromDBBaseOnStatus(String email , String status){
        return OrderMapper.getOrdersFromDBBaseOnStatus(email, status);
    }

    public static List<String> getUserCartFromDB(String email){
        return OrderMapper.getUserCartFromDB(email);
    }

    public static List<Integer> getDeliveredOrderId(String email){
        return OrderMapper.getDeliveredOrderId(email);
    }

    public static JSONObject getDeliveredOrdersFromDBById(String email, int orderId) throws ParseException {
        return OrderMapper.getDeliveredOrdersFromDBById(email, orderId);
    }
}

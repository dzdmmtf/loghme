package com.loghme.repository;

import com.loghme.controllers.OrderServlet;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Delivery {
    static JSONArray deliveryList = new JSONArray();
    static JSONObject deliveryInfo = new JSONObject();
    static String status = "searching";

    public static JSONArray getDeliveryList() {
        return deliveryList;
    }

    public static JSONObject getDeliveryInfo() {
        return deliveryInfo;
    }

    public static String getStatus(){
        return status;
    }

    public static void setStatus(String status){
        Delivery.status = status;
    }

    public static void setDeliveryList(JSONArray deliveryList) {
        Delivery.deliveryList = deliveryList;
    }

    public static String calculateTimeOfDeliveries(JSONObject restaurantLocation)throws ParseException{
        Map<String, Double> deliveriesTime = new HashMap<String, Double>();
        Long userLocationX = 00000l;
        Long userLocationY = 00000l;
        Long restaurantLocationX = (Long)restaurantLocation.get("x");
        Long restaurantLocationY = (Long)restaurantLocation.get("y");
        for(int i=0; i < deliveryList.size(); i++) {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(deliveryList.get(i).toString());
            String deliveryId = (String) jsonObject.get("id");
            JSONObject deliveryLocationObj = (JSONObject) jsonObject.get("location");
            Long deliveryVelocity = (Long) jsonObject.get("velocity");
            Long deliveryLocationX = (Long) deliveryLocationObj.get("x");
            Long deliveryLocationY = (Long) deliveryLocationObj.get("y");
            double distance = calculateDistance(userLocationX, userLocationY, restaurantLocationX, restaurantLocationY,
                    deliveryLocationX, deliveryLocationY);
            deliveriesTime.put(deliveryId, (distance / deliveryVelocity));
        }
        if(deliveryList.size()!=0) {
            Double minTime = Collections.min(deliveriesTime.values());
            String bikeDeliveryId = getAcceptedBikeDelivery(deliveriesTime, minTime);
            deliveryInfo.put("id", bikeDeliveryId);
            deliveryInfo.put("arriving time", Math.round(minTime));
            return bikeDeliveryId;
        }
        deliveryInfo.put("id", "0");
        deliveryInfo.put("arriving time", 0);
        return "0";
    }

    public static String getAcceptedBikeDelivery(Map<String, Double> deliveriesTime, Double minTime){
        Iterator it = deliveriesTime.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry obj = (Map.Entry)it.next();
            if((Double) obj.getValue() == minTime)
                return (String) obj.getKey();
        }
        return "";
    }

    public static double calculateDistance(Long userLocationX, Long userLocationY, Long restaurantLocationX,
                                           Long restaurantLocationY, Long deliveryLocationX, Long deliveryLocationY){
        double part1X = Math.pow(restaurantLocationX.intValue() - userLocationX.intValue(), 2);
        double part1Y = Math.pow(restaurantLocationY.intValue() - userLocationY.intValue(), 2);
        double part2X = Math.pow(deliveryLocationX.intValue() - restaurantLocationX.intValue(), 2);
        double part2Y = Math.pow(deliveryLocationY.intValue() - restaurantLocationY.intValue(), 2);
        return Math.sqrt(part1X + part1Y) + Math.sqrt(part2X + part2Y);
    }
}
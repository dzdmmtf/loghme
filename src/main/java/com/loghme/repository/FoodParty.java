package com.loghme.repository;

import com.loghme.database.FoodMapper;
import com.loghme.database.FoodPartyMapper;
import com.loghme.database.RestaurantMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

public class FoodParty {
    static JSONArray partyFoods = new JSONArray();
    static JSONObject restaurantInfo = new JSONObject();

    public static String getFoodCount(String restaurantId, String foodName) throws ParseException {
        return FoodPartyMapper.getFoodCount(restaurantId, foodName);
    }

    public static void updateFoodCount(String restaurantId, String foodName ,boolean isAdd, String orderAmount) throws ParseException{
        FoodPartyMapper.updateFoodCount(restaurantId, foodName, isAdd, orderAmount);
    }

    public static void updateRestaurantMenu(String restaurantId){
        FoodPartyMapper.addFoodPartyFoodToFoodTable(restaurantId);
    }

    public static JSONArray retrievePartyFoods() throws IOException, ParseException {
        JSONArray foodPartyFood = new JSONArray();
        URL obj = new URL(Config.FOODPARTYAPI);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuilder response = new StringBuilder();
            while ((inputLine = in.readLine()) != null)
                response.append(inputLine);
            in.close();
            JSONParser parser = new JSONParser();
            foodPartyFood = (JSONArray) parser.parse(response.toString());
        }

        return foodPartyFood;
    }

    public static JSONArray getFoodPartyList(){
        return FoodPartyMapper.getFoodPartyInfoFromDB();
    }

    public static String getRestaurantName(String restaurantId) throws ParseException {
        return FoodPartyMapper.getRestaurantNameByRestaurantId(restaurantId);
    }

    public static String getFoodName(String restaurantId){
        return  FoodPartyMapper.getFoodNameByRestaurantId(restaurantId);
    }

    public static void addFoodPartRestToRests(String restaurantId) throws ParseException {
        FoodPartyMapper.addFoodPartyRestToRestsTable(restaurantId);
        FoodPartyMapper.addFoodPartyFoodToFoodTable(restaurantId);

    }

    public static boolean validateOrderedAmount(String restaurantId,String foodName,String orderAmount) throws ParseException {
        return FoodPartyMapper.validateOrderedAmount(restaurantId, foodName, orderAmount);
    }

    public static void externalServerConnection() throws ParseException, IOException {
        JSONArray foodPartyFood = retrievePartyFoods();
        new FoodPartyMapper();
        FoodPartyMapper.insertFoodPartyRestaurantsToDB(foodPartyFood);
        FoodPartyMapper.insertFoodPartyFoodsToDB(foodPartyFood);
        System.out.println("---FOOD PARTY :: DONE---");
    }
}

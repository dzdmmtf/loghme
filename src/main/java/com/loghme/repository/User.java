package com.loghme.repository;

import com.loghme.database.DataMapperException;
import com.loghme.database.UserMapper;
import org.json.simple.JSONObject;

public class User {
    static String name;
    static String lastName;
    static String phone;
    static String email;
    static int credit;
    static int orderId;
    static int sessionId = 0;

    public static int getOrderId() {
        return orderId;
    }

    public static int getCredit() {
        return credit;
    }

    public static int getSessionId(){return sessionId;}

    public static String getEmail() {
        return email;
    }

    public static String getLastName() {
        return lastName;
    }

    public static String getName() {
        return name;
    }

    public static String getPhone() {
        return phone;
    }

    public static void setName(String name){
        User.name = name;
    }

    public static void setLastName(String lastName){
        User.lastName = lastName;
    }

    public static void setPhone(String phone){
        User.phone = phone;
    }

    public static void setEmail(String email){
        User.email = email;
    }

    public static void setCredit(int credit) {
        User.credit = credit;
    }

    public static void setOrderId(int orderId) {
        User.orderId = orderId;
    }

    public static void setSessionId(int sessionId){User.sessionId = sessionId;}

    public static void updateUserCredit(String email, int updatedCredit){
        UserMapper.updateUserCredit(email, updatedCredit);
    }

    public static void createUserTable(){
        UserMapper.createTable();
    }

    public static boolean checkUserExistence(String email){
        return UserMapper.checkUserExistence(email);
    }

    public static void insertUser(String name, String familyName, String email, String phone, String password)
            throws DataMapperException {
        UserMapper.insertUser(name, familyName, email, phone, password);
    }

    public static boolean authenticationByPass(String email, String password){
        return UserMapper.authenticationByPass(email, password);
    }

    public static JSONObject retrieveUserInfoByEmail(String email){
        return UserMapper.retrieveUserInfoByEmail(email);
    }

    public static void setUser(JSONObject userInfo) {
        User.setName((String) userInfo.get("name"));
        User.setLastName((String) userInfo.get("familyName"));
        User.setEmail((String) userInfo.get("email"));
        User.setPhone((String) userInfo.get("phone"));
        User.setCredit((int) userInfo.get("credit"));
        User.setOrderId((int) (Math.random() * ((10000) + 1)));
        User.setSessionId((int) (Math.random() * ((10000) + 10)));
    }
}
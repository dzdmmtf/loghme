package com.loghme.repository;

import com.loghme.database.FoodMapper;
import com.loghme.database.OrderMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.util.*;

public class Cart {
    static List<String> userCart = new ArrayList<>();
    static JSONObject userOrder = new JSONObject();
    static JSONArray olderUsersOrder = new JSONArray();

    public static String getOrderPrice(JSONObject restaurantTotalInfo, String foodName) throws ParseException{
        JSONArray menu = (JSONArray)restaurantTotalInfo.get("menu");
        JSONObject jsonObjMenu;
        String price = "0";
        for (int i = 0; i < menu.size(); i++) {
            JSONParser parser = new JSONParser();
            jsonObjMenu = (JSONObject) parser.parse(menu.get(i).toString());
            String name = (String) jsonObjMenu.get("name");
            if (name.equals(foodName)) {
                price = (String) jsonObjMenu.get("price");
                return price;
            }
        }
        return "0";
    }

    public static boolean validCart(String restaurantID) {
        setUserCart(Order.getUserCartFromDB(User.getEmail()));
        for (int i = 1; i < userCart.size(); i += 2)
            if (!userCart.get(i).equals(restaurantID))
                return false;
        return true;
    }

    public static void getCart() {
        userOrder.clear();
        setUserCart(Order.getUserCartFromDB(User.getEmail()));
        System.out.println("USER CART" + userCart);
        for(int i=0; i<userCart.size(); i+=2) {
            if(userOrder.containsKey(userCart.get(i))) {
                Integer num = (Integer) userOrder.get(userCart.get(i));
                userOrder.put(userCart.get(i), num+1);
            } else {
                userOrder.put(userCart.get(i), 1);
            }
        }
    }

    public static JSONObject getJsonCart() throws ParseException, InterruptedException {
        JSONObject orderedInfo = new JSONObject();
        getCart();
        if(getUserCart().size() > 1) {
            int totalPrice = 0;
            String restaurantName;
            restaurantName = Restaurant.getRestaurantName(Cart.getUserCart().get(1));
            JSONArray orderedFoodInfo = new JSONArray();
            JSONObject orders= Cart.getUserOrder();
            Set<String> ordersFood = orders.keySet();
            Iterator<String> ordersFoodIterate = ordersFood.iterator();
            int orderedFoodId = 0;
            while(ordersFoodIterate.hasNext()){
                String key = ordersFoodIterate.next();
                String value = orders.get(key).toString();
                JSONObject orderedItem = new JSONObject();
                String price = FoodMapper.getFoodPrice(Cart.getUserCart().get(1), key);
                orderedItem.put("name", key);
                orderedItem.put("amount", value);
                orderedItem.put("price", price);
                orderedItem.put("orderedFoodId", Integer.toString(orderedFoodId));
                orderedItem.put("deliveryStatus","searching");
                orderedFoodId +=1 ;
                orderedFoodInfo.add(orderedItem);
                totalPrice += Integer.parseInt(value)*Integer.parseInt(price);
            }
            int num = Cart.getOrdersNum();
            orderedInfo.put("restaurantName", restaurantName);
            orderedInfo.put("order", orderedFoodInfo);
            orderedInfo.put("totalPrice", totalPrice);
            orderedInfo.put("restaurantId", Cart.getUserCart().get(1));
            orderedInfo.put("ordersNum", num);
        } else {
//            System.out.println("User cart is empty!");
            orderedInfo.put("ordersNum", 0);
            orderedInfo.put("totalPrice", 0);
        }
        return orderedInfo;
    }

    public static JSONObject getOrderInfo(String restaurantId, String foodName, int foodCount) throws ParseException, InterruptedException {
        JSONObject orderedInfo = new JSONObject();
//        JSONObject restaurantTotalInfo = Restaurant.getRestaurant(restaurantId);
//        String price = Cart.getOrderPrice(restaurantTotalInfo, foodName);
        String price = FoodMapper.getFoodPrice(restaurantId, foodName);
        orderedInfo.put("name", foodName);
        orderedInfo.put("amount", foodCount);
        orderedInfo.put("price", price);
        return orderedInfo;
    }

    public static int calculatePrice(JSONObject orderedInfo){
        int count = (Integer) orderedInfo.get("amount");
        String price = (String) orderedInfo.get("price");
        return count*Integer.parseInt(price);
    }

    public static int getOrdersNum(){
        int sum = 0;
        JSONObject orders= Cart.getUserOrder();
        Set<String> ordersFood = orders.keySet();
        Iterator<String> ordersFoodIterate = ordersFood.iterator();
        while(ordersFoodIterate.hasNext()){
            String key = ordersFoodIterate.next();
            String value = orders.get(key).toString();
            sum += Integer.parseInt(value);
        }
        return sum;
    }

    public static List<String> getUserCart() {
        return userCart;
    }

    public static JSONObject getUserOrder() {
        return userOrder;
    }


    public static void setUserCart(List<String> userCart) {
        Cart.userCart = userCart;
    }

    public static JSONArray getOlderUsersOrder(){
        olderUsersOrder.clear();
        olderUsersOrder = OrderMapper.getOlderOrder(User.getEmail());
        return olderUsersOrder;
    }

}
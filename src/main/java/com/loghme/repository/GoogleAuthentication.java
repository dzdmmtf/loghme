package com.loghme.repository;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;

import java.io.IOException;

public class GoogleAuthentication {

    public static GoogleIdToken.Payload check(String tokenString) {
        JsonFactory jsonFactory = new JacksonFactory();
        HttpTransport httpTransport = new NetHttpTransport();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(httpTransport, jsonFactory).build();
        GoogleIdToken.Payload payload = null;
        try {
            GoogleIdToken token = GoogleIdToken.parse(jsonFactory, tokenString);

            if (token == null || token.getPayload() == null)
                return null;

            if (token.getPayload().getEmailVerified())
                payload = token.getPayload();

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println(payload);
        return payload;
    }
}

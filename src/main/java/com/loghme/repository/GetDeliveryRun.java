package com.loghme.repository;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class GetDeliveryRun implements Runnable{
    @Override
    public void run(){
        try {
            Delivery.setDeliveryList(new JSONArray());
            URL url = new URL(Config.DELIVERYAPI);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            int responseCode = con.getResponseCode();
//            System.out.println("\nSending 'GET' request to URL : " + url);
//            System.out.println("Response Code : " + responseCode);
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer response = new StringBuffer();

                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();
                JSONParser parser = new JSONParser();
                Delivery.setDeliveryList((JSONArray) parser.parse(response.toString())); ;
            }
//            System.out.println(Delivery.getDeliveryList());
//            System.out.println("_____________________________________________________________________");
        } catch (IOException | ParseException e) {e.printStackTrace();}
    }
}
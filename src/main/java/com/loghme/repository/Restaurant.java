package com.loghme.repository;

import com.loghme.database.FoodMapper;
import com.loghme.database.RestaurantMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Restaurant {

    public static double calculateDistance(JSONObject location) {
        Long x = (Long) location.get("x");
        Long y = (Long) location.get("y");
        double x2 = Math.pow(x, 2);
        double y2 = Math.pow(y, 2);
        return Math.sqrt(x2 + y2);
    }

    public static JSONArray getRestaurants(String page, String count) throws ParseException {
        JSONArray restaurants = new JSONArray();
        Integer p = Integer.parseInt(page);
        Integer c = Integer.parseInt(count);
        System.out.println("in getRestaurants");
        JSONArray restaurantList = RestaurantMapper.findPagination(p, c);
        System.out.println("pagination::: "+restaurantList);
        for (Object o : restaurantList) {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(o.toString());
            JSONObject location = (JSONObject) parser.parse((String) jsonObject.get("location"));
            double distance = calculateDistance(location);
            if (distance <= 170)
                restaurants.add(jsonObject);
        }
        return restaurants;
    }

    public static JSONObject getRestaurant(String restaurantId) throws ParseException, InterruptedException {
        JSONObject statusCodeJson = new JSONObject();
        JSONObject restaurant = RestaurantMapper.find(restaurantId);
        if(restaurant.isEmpty()) {
            statusCodeJson.put("statusCode", "NOT_FOUND");
            return statusCodeJson;
        } else {
            JSONParser parser = new JSONParser();
            JSONObject location = (JSONObject) parser.parse((String) restaurant.get("location"));
            double distance = Restaurant.calculateDistance(location);
            if(distance > 170) {
                statusCodeJson.put("statusCode", "FORBIDDEN");
                return statusCodeJson;
            } else {
                JSONArray menu = FoodMapper.findRestaurantFood(restaurantId);
                restaurant.put("menu", menu);
                restaurant.put("statusCode", "OK");
                TimeUnit.SECONDS.sleep(1);
                return restaurant;
            }
        }
    }

    public static JSONArray getRestaurantsInfo() throws IOException, ParseException {
        JSONArray jsonArray = new JSONArray();
        URL obj = new URL(Config.RESTAURANTSAPI);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");

        if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            JSONParser parser = new JSONParser();
            jsonArray = (JSONArray) parser.parse(response.toString());

        }
        return jsonArray;
    }

    public static void externalServerConnection() throws ParseException, IOException {
        System.out.println("WELCOME!");
        JSONArray restaurants = getRestaurantsInfo();
        System.out.println("hi");
        new RestaurantMapper();
        new FoodMapper();
        RestaurantMapper.insert(restaurants);
        FoodMapper.insert(restaurants);
        System.out.println("---DONE------");
    }

    public static String getRestaurantName(String restaurantID) throws ParseException {
        return RestaurantMapper.getName(restaurantID);
    }

    public static String getFoodNameById(String foodId) {
        return FoodMapper.findName(foodId);
    }

    public static boolean validRestaurantAndFood(String restaurantID, String foodName) {
        boolean valid = true;
        if (!restaurantExists(restaurantID)) {
            System.out.println("This restaurant doesn't exist!");
            valid = false;
        } else if (!restaurantHasFood(restaurantID, foodName)) {
            System.out.println("This restaurant doesn't have such food!!");
            valid = false;
        }
        return valid;
    }

    public static boolean restaurantExists(String restaurantID) {
        JSONObject restaurant = RestaurantMapper.find(restaurantID);
        System.out.println("res exists: " + !restaurant.isEmpty());
        return !restaurant.isEmpty();
    }

    public static boolean restaurantHasFood(String restaurantID, String foodName) {
        return FoodMapper.existsFoodName(restaurantID, foodName);
    }

    public static JSONObject searchByName(String name) {
        return RestaurantMapper.findByName(name);
    }

    public static JSONArray searchByFood(String name) {
        JSONArray jsonArray = new JSONArray();
        List restaurants = FoodMapper.findByName(name);
        for (Object restaurant : restaurants) {
            JSONObject jsonObject = RestaurantMapper.find((String) restaurant);
            jsonArray.add(jsonObject);
        }
        return jsonArray;
    }
}

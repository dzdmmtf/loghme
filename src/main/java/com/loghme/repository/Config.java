package com.loghme.repository;

public class Config {
    public static final String RESTAURANTSAPI = "http://138.197.181.131:8080/restaurants";
    public static final String DELIVERYAPI = "http://138.197.181.131:8080/deliveries";
    public static final String FOODPARTYAPI = "http://138.197.181.131:8080/foodparty";
    public static final String CLIENT_ID = "794254052264-udn3vhgoq3odjtnalju7nooshhvda0eo.apps.googleusercontent.com";
}
package com.loghme.repository;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JsonWebToken {
    private static String strongKey = "Loghme";

    public static Date getTomorrowDate(Date currentDate){
        Calendar cal = Calendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.HOUR_OF_DAY, 24);
        Date tomorrowDate = cal.getTime();
        return tomorrowDate;
    }

    public static String createToken(String email){
        Date currentDate = new Date();
        Map<String, Object> headerClaims = new HashMap();
        headerClaims.put("alg", "HS256");
        headerClaims.put("typ", "JWT");

        Algorithm algorithm = Algorithm.HMAC256(strongKey);
        String jwt = JWT.create()
                .withHeader(headerClaims)
                .withSubject("Loghme Authorization")
                .withIssuer("loghme-sec-api")
                .withIssuedAt(currentDate)
                .withExpiresAt(getTomorrowDate(currentDate))
                .withClaim("userId", email)
                .sign(algorithm);

        return jwt;
    }
}
package com.loghme.database;

import org.json.simple.JSONObject;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public final class UserMapper {

    private static final String TABLE_NAME = "USER";
    public UserMapper(){
        ConnectionPool.setupDataSource();
    }

    public static void createTable() throws DataMapperException {
        try {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            st.executeUpdate(String.format(
                    "CREATE TABLE IF NOT EXISTS %s " +
                            "(" +
                            "name TEXT," +
                            "familyName TEXT," +
                            "email VARCHAR(512)," +
                            "phone TEXT," +
                            "credit integer," +
                            "password TEXT," +
                            "PRIMARY KEY(email)" +
                            ");",
                    TABLE_NAME));
            st.close();
            con.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean checkUserExistence(String email){
        int count = 0;
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT COUNT(*) AS COUNT FROM USER WHERE email = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                count = result.getInt(1);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if(count == 0)
            return false;
        return true;
    }

    public static void insertUser(String name, String familyName, String email, String phone, String password)
            throws DataMapperException {
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "INSERT INTO USER (name , familyName, email, phone, credit, password) VALUES (?, ?, ?, ?, 0, ?)";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, name);
            statement.setString(2, familyName);
            statement.setString(3, email);
            statement.setString(4, phone);
            statement.setString(5, password);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean authenticationByPass(String email, String password){
        int count = 0;
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT COUNT(*) AS COUNT FROM USER WHERE email = ? AND password = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            statement.setString(2, password);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                count = result.getInt(1);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count != 0;
    }

    public static JSONObject retrieveUserInfoByEmail(String email){
        JSONObject userInfo = new JSONObject();
        String name = "";
        String familyName = "";
        String phone = "";
        int credit = 0;
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT name, familyName, phone, credit FROM USER WHERE email = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                name = result.getString(1);
                familyName = result.getString(2);
                phone = result.getString(3);
                credit = result.getInt(4);
            }
            userInfo.put("name", name);
            userInfo.put("familyName", familyName);
            userInfo.put("email", email);
            userInfo.put("phone", phone);
            userInfo.put("credit", credit);
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userInfo;
    }

    public static void updateUserCredit(String email, int updatedCredit){
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "UPDATE USER SET credit = ? WHERE email = ?" ;
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setInt(1, updatedCredit);
            statement.setString(2, email);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
package com.loghme.database;

import java.sql.Connection;
import java.sql.SQLException;
import com.mchange.v2.c3p0.ComboPooledDataSource;


public class ConnectionPool {

    public static ComboPooledDataSource dataSource;

    public static void setupDataSource() {

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("TEST DB");
        dataSource = new ComboPooledDataSource();
        dataSource.setJdbcUrl("jdbc:mysql://mysql-service:3306/loghme?allowPublicKeyRetrieval=true&useSSL=false&useUnicode=yes&characterEncoding=UTF-8");
        dataSource.setUser("root");
        dataSource.setPassword("password");
    }

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }
}

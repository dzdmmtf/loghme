package com.loghme.database;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public final class FoodPartyMapper {
    private static final String TABLE_NAME_RESTAURANT = "FOOD_PARTY_RESTAURANT";
    private static final String TABLE_NAME_FOOD = "FOOD_PARTY_FOOD";
    public FoodPartyMapper(){
        dropFoodPartyFoodTable();
        dropFoodPartyRestaurantTable();
        createFoodPartyRestaurantTable();
        createFoodPartyFoodTable();
    }

    public static void createFoodPartyRestaurantTable(){
        try {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            st.executeUpdate(String.format(
                    "CREATE TABLE %s " +
                            "(" +
                            "restaurantId VARCHAR(255)," +
                            "name TEXT," +
                            "location TEXT,"+
                            "logo TEXT,"+
                            "PRIMARY KEY(restaurantId)" +
                            ");",
                    TABLE_NAME_RESTAURANT));
            st.close();
            con.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void createFoodPartyFoodTable(){
        try {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            st.executeUpdate(String.format(
                    "CREATE TABLE %s " +
                            "(" +
                            "id VARCHAR(255),"+
                            "restaurantId VARCHAR(255)," +
                            "name TEXT," +
                            "price TEXT,"+
                            "oldPrice TEXT,"+
                            "count TEXT,"+
                            "image TEXT," +
                            "description TEXT,"+
                            "popularity TEXT," +
                            "PRIMARY KEY(Id)," +
                            "FOREIGN KEY (restaurantId) references FOOD_PARTY_RESTAURANT(restaurantId)"+
                            ");",
                    TABLE_NAME_FOOD));
            st.close();
            con.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void dropFoodPartyRestaurantTable(){
        try {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME_RESTAURANT));
            st.close();
            con.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void dropFoodPartyFoodTable(){
        try {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            st.executeUpdate(String.format("DROP TABLE IF EXISTS %s", TABLE_NAME_FOOD));
            st.close();
            con.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertFoodPartyRestaurantsToDB(JSONArray foodPartyFood) {
        Connection conn;
        PreparedStatement pStatement;
        String INSERT_USERS_SQL = "INSERT INTO FOOD_PARTY_RESTAURANT (restaurantId, name, location, logo) values (?, ?, ?, ?) " +
                "on duplicate key update name=values(name)";

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();

            pStatement = conn.prepareStatement(INSERT_USERS_SQL);
            conn.setAutoCommit(false);
            for (Object o : foodPartyFood) {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(o.toString());
                pStatement.setString(1, (String) jsonObject.get("id"));
                pStatement.setString(2, (String) jsonObject.get("name"));
                pStatement.setString(3, jsonObject.get("location").toString());
                pStatement.setString(4, (String) jsonObject.get("logo"));
                pStatement.addBatch();
            }
            pStatement.executeBatch();
            conn.commit();
            conn.setAutoCommit(true);

            pStatement.close();
            conn.close();
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
    }

    public static void insertFoodPartyFoodsToDB(JSONArray foodPartyFood) throws DataMapperException {
        Integer i;
        Connection conn;
        PreparedStatement pStatement;
        String INSERT_USERS_SQL = "INSERT INTO FOOD_PARTY_FOOD (id, name, price, oldPrice, count, description, image, popularity, restaurantId) " +
                "values (?, ?, ?, ?, ?, ?, ?, ?, ?) on duplicate key update id=values(id)";
        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();

            pStatement = conn.prepareStatement(INSERT_USERS_SQL);
            conn.setAutoCommit(false);
            for (Object restaurant : foodPartyFood) {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(restaurant.toString());
                JSONArray jsonArray = (JSONArray) jsonObject.get("menu");
                for (i = 0; i < jsonArray.size(); i++) {
                    JSONObject foodObject = (JSONObject) parser.parse(jsonArray.get(i).toString());
                    pStatement.setString(1, jsonObject.get("id").toString() + i.toString());
                    pStatement.setString(2, (String) foodObject.get("name"));
                    pStatement.setString(3,  foodObject.get("price").toString());
                    pStatement.setString(4,  foodObject.get("oldPrice").toString());
                    pStatement.setString(5, foodObject.get("count").toString());
                    pStatement.setString(6, (String) foodObject.get("description"));
                    pStatement.setString(7, (String) foodObject.get("image"));
                    pStatement.setString(8, foodObject.get("popularity").toString());
                    pStatement.setString(9, jsonObject.get("id").toString());
                    pStatement.addBatch();
                }
            }

            pStatement.executeBatch();
            conn.commit();
            conn.setAutoCommit(true);

            pStatement.close();
            conn.close();
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
    }

    public static String getFoodNameByRestaurantId(String restaurantId){
        String foodName = "";
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT name FROM FOOD_PARTY_FOOD WHERE restaurantId = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                foodName = result.getString(1);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return foodName;
    }

    public static boolean validateOrderedAmount(String restaurantId,String foodName,String orderAmount){
        String count = getFoodCount(restaurantId, foodName);
        if(Integer.parseInt(count) - Integer.parseInt(orderAmount) >=0)
            return true;
        else
            return false;
    }

    public static String getRestaurantNameByRestaurantId(String restaurantId){
        String restaurantName = "";
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT name FROM FOOD_PARTY_RESTAURANT WHERE restaurantId = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                restaurantName = result.getString(1);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return restaurantName;
    }

    public static JSONArray getFoodPartyMenuByRestaurantId(String restaurantId){
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT id, name, price, oldPrice, count, image, description, popularity FROM FOOD_PARTY_FOOD WHERE restaurantId = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                jsonObject.put("id", result.getString(1));
                jsonObject.put("name", result.getString(2));
                jsonObject.put("price", result.getString(3));
                jsonObject.put("oldPrice", result.getString(4));
                jsonObject.put("count", result.getString(5));
                jsonObject.put("image", result.getString(6));
                jsonObject.put("description", result.getString(7));
                jsonObject.put("popularity", result.getString(8));
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        jsonArray.add(jsonObject);
        return jsonArray;
    }

    public static JSONArray getFoodPartyInfoFromDB(){
        JSONArray foodPartyList = new JSONArray();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT restaurantId, name, location, logo FROM FOOD_PARTY_RESTAURANT";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                JSONObject obj = new JSONObject();
                obj.put("id", result.getString(1));
                obj.put("name", result.getString(2));
                obj.put("location", result.getString(3));
                obj.put("logo", result.getString(4));
                obj.put("menu", getFoodPartyMenuByRestaurantId(result.getString(1)));
                foodPartyList.add(obj);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return foodPartyList;
    }

    public static String getFoodCount(String restaurantId, String foodName){
        String count = "0";
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT count FROM FOOD_PARTY_FOOD WHERE restaurantId = ? AND name = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            statement.setString(2, foodName);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                count = result.getString(1);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static void updateFoodCount(String restaurantId, String foodName ,boolean isAdd, String orderAmount){
        String count = getFoodCount(restaurantId, foodName);
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "UPDATE FOOD_PARTY_FOOD SET count = ? WHERE restaurantId = ? AND name = ?" ;
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            Integer newValue;
            if(isAdd)
                newValue = (Integer.parseInt(count) - Integer.parseInt(orderAmount));
            else
                newValue = (Integer.parseInt(count) + Integer.parseInt(orderAmount));
            statement.setString(1, newValue.toString());
            statement.setString(2, restaurantId);
            statement.setString(3, foodName);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getRestaurantInfoByRestaurantId(String restaurantId){
        JSONObject restaurantInfo = new JSONObject();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT name, location, logo FROM FOOD_PARTY_RESTAURANT WHERE restaurantId = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                restaurantInfo.put("restaurantId", restaurantId);
                restaurantInfo.put("name", result.getString(1));
                restaurantInfo.put("location", result.getString(2));
                restaurantInfo.put("logo", result.getString(3));
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return restaurantInfo;
    }

    public static void addFoodPartyRestToRestsTable(String restaurantId){
        JSONObject restaurantInfo = getRestaurantInfoByRestaurantId(restaurantId);
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "INSERT INTO Restaurant (id, name, location, logo) values (?, ?, ?, ?) on duplicate key update name=values(name)";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            statement.setString(2, (String) restaurantInfo.get("name"));
            statement.setString(3, (String) restaurantInfo.get("location"));
            statement.setString(4, (String) restaurantInfo.get("logo"));
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void addFoodPartyFoodToFoodTable(String restaurantId){
        JSONArray restaurantMenuInfo = getFoodPartyMenuByRestaurantId(restaurantId);
        JSONObject menu = (JSONObject) restaurantMenuInfo.get(0);
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "INSERT INTO Food (id, name, price, image, description, popularity, restaurantId) values (?, ?, ?, ?, ?, ?, ?) " +
                    "on duplicate key update id=values(id)";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, menu.get("id")+"$");
            statement.setString(2, (String)menu.get("name"));
            statement.setString(3, (String)menu.get("price"));
            statement.setString(4, (String)menu.get("image"));
            statement.setString(5, (String)menu.get("description"));
            statement.setString(6, (String)menu.get("popularity"));
            statement.setString(7, restaurantId);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}

-- CREATE DATABASE loghme;
-- USE loghme;

CREATE TABLE if NOT EXISTS Restaurant (
    id varchar(255) primary key,
    name text,
    location text,
    logo text
    );

CREATE TABLE if NOT EXISTS Food (
    id varchar(255) primary key,
    name text,
    price text,
    image text,
    description text,
    popularity text,
    restaurantId varchar(255),
    foreign key (restaurantId) references Restaurant(id)
    );

DROP TABLE IF EXISTS FOOD_PARTY_FOOD;

DROP TABLE IF EXISTS FOOD_PARTY_RESTAURANT;

CREATE TABLE FOOD_PARTY_RESTAURANT(
    restaurantId VARCHAR(255),
    name TEXT,
    location TEXT,
    logo TEXT,
    PRIMARY KEY(restaurantId)
    );

CREATE TABLE FOOD_PARTY_FOOD(
    id VARCHAR(255),
    restaurantId VARCHAR(255),
    name TEXT,
    price TEXT,
    oldPrice TEXT,
    count TEXT,
    image TEXT,
    description TEXT,
    popularity TEXT,
    PRIMARY KEY(Id),
    FOREIGN KEY (restaurantId) references FOOD_PARTY_RESTAURANT(restaurantId)
    );

CREATE TABLE IF NOT EXISTS USER (
    name TEXT,
    familyName TEXT,
    email VARCHAR(512),
    phone TEXT,
    credit integer,
    password TEXT,
    PRIMARY KEY(email)
    );

CREATE TABLE IF NOT EXISTS USER_ORDER(
    email VARCHAR(255),
    foodCount integer,
    foodName VARCHAR(255),
    restaurantId TEXT,
    status VARCHAR(255),
    orderId integer,
    restaurantName TEXT,
    PRIMARY KEY(email, foodName, status)
    );

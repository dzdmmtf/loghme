package com.loghme.database;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public final class FoodMapper {
    private static final String TABLE_NAME = "Food";

    public FoodMapper() {
        Statement statement;
        Connection conn;
        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();

            statement = conn.createStatement();
            statement.executeUpdate(String.format(
                    "create table if not exists %s (id varchar(255) primary key, name text, price text, image text, " +
                            "description text, popularity text, restaurantId varchar(255), foreign key (restaurantId) references Restaurant(id));", TABLE_NAME));
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insert(JSONArray restaurants) throws DataMapperException {
        Integer i;
        Connection conn;
        PreparedStatement pStatement;
        String INSERT_USERS_SQL = "INSERT INTO Food (id, name, price, description, image, popularity, restaurantId) " +
                "values (?, ?, ?, ?, ?, ?, ?) on duplicate key update id=values(id)";
        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();

            pStatement = conn.prepareStatement(INSERT_USERS_SQL);
            conn.setAutoCommit(false);
            for (Object restaurant : restaurants) {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(restaurant.toString());
                JSONArray jsonArray = (JSONArray) jsonObject.get("menu");
                for (i = 0; i < jsonArray.size(); i++) {
                    JSONObject foodObject = (JSONObject) parser.parse(jsonArray.get(i).toString());
                    pStatement.setString(1, jsonObject.get("id").toString() + i.toString());
                    pStatement.setString(2, (String) foodObject.get("name"));
                    pStatement.setString(3, foodObject.get("price").toString());
                    pStatement.setString(4, (String) foodObject.get("description"));
                    pStatement.setString(5, (String) foodObject.get("image"));
                    pStatement.setString(6, foodObject.get("popularity").toString());
                    pStatement.setString(7, jsonObject.get("id").toString());
                    pStatement.addBatch();
                }
            }

            pStatement.executeBatch();
            conn.commit();
            conn.setAutoCommit(true);

            pStatement.close();
            conn.close();
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
    }

    public static boolean existsFoodName(String restaurantId, String foodName) throws DataMapperException {
        Connection conn;
        boolean found = false;

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();
            String sqlQuery = "SELECT * FROM Food WHERE restaurantId = ? and name = ?";
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            statement.setString(2, foodName);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                found = true;
            }
            result.close();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return found;
    }

    public static JSONArray findRestaurantFood(String restaurantId) throws DataMapperException {
        JSONArray jsonArray = new JSONArray();
        Connection conn;

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();
            String sqlQuery = "SELECT * FROM Food WHERE restaurantId = ?";
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                JSONObject jsonObject = new JSONObject();
                String foodId = result.getString("id");
                String name = result.getString("name");
                String image = result.getString("image");
                String price = result.getString("price");
                String popularity = result.getString("popularity");
                String description = result.getString("description");
                jsonObject.put("foodId", foodId);
                jsonObject.put("name", name);
                jsonObject.put("image", image);
                jsonObject.put("price", price);
                jsonObject.put("popularity", popularity);
                jsonObject.put("description", description);
                jsonArray.add(jsonObject);
            }
            result.close();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public static String findName(String foodId) throws DataMapperException {
        Connection conn;
        String name = "";
        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();
            String sqlQuery = "SELECT * FROM Food WHERE id = ?";
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, foodId);
            ResultSet result = statement.executeQuery();
            result.next();
            name = result.getString("name");
            result.close();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return name;
    }

    public static List findByName(String name) throws DataMapperException {
        Connection conn;
        List restaurants = new ArrayList<String>();
        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();
            String sqlQuery = "SELECT * FROM Food WHERE name = ?";
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                String restaurantId = result.getString("restaurantId");
                restaurants.add(restaurantId);
            }
            result.close();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return restaurants;
    }

    public static String getFoodPrice(String restaurantId, String foodName){
        String price = "";
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT price FROM Food WHERE restaurantId = ? AND name = ?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            statement.setString(2, foodName);
            ResultSet result = statement.executeQuery();
            while(result.next()) {
                price = result.getString(1);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return price;
    }
}

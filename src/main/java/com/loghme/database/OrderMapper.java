package com.loghme.database;
import com.loghme.repository.Cart;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


public final class OrderMapper {
    private static final String TABLE_NAME = "USER_ORDER";
    public OrderMapper(){
        ConnectionPool.setupDataSource();
    }

    public static void createTable() throws DataMapperException {
        try {
            Connection con = ConnectionPool.getConnection();
            Statement st = con.createStatement();
            st.executeUpdate(String.format(
                    "CREATE TABLE IF NOT EXISTS %s " +
                            "(" +
                            "email VARCHAR(255)," +
                            "foodCount integer," +
                            "foodName VARCHAR(255),"+
                            "restaurantId TEXT,"+
                            "status VARCHAR(255)," +
                            "orderId integer," +
                            "restaurantName TEXT," +
                            "PRIMARY KEY(email, foodName, status)" +
                            ");",
                    TABLE_NAME));
            st.close();
            con.close();
        }catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static List<String> getUserCartFromDB(String email){
        List<String> userCart = new ArrayList<>();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT restaurantId, status, foodName, foodCount FROM USER_ORDER WHERE email = ? AND status='finding'";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String restaurantId = result.getString(1);
                String foodName = result.getString(3);
                int foodCount = result.getInt(4);
                for(int i=0; i< foodCount;i++) {
                    userCart.add(foodName);
                    userCart.add(restaurantId);
                }
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userCart;
    }

    public static void addNewItemToUserOrderDB(String restaurantName, String restaurantId, String foodName, String email, int orderId){
        int foodCount = getFoodCountOrder(foodName, email);
        if(foodCount == 0)
            insertItemRow(restaurantName, restaurantId, foodName, email, orderId);
        else{
            //increase food count
            updateFoodCount(restaurantId, foodName, email, foodCount+1);
        }
    }

    public static void decOrDelItemFromUserOrderDB(String restaurantId, String foodName, String email){
        int foodCount = getFoodCountOrder(foodName, email);
        if(foodCount == 1)
            deleteItemRow(restaurantId, foodName, email);
        else{
            //decrease food count
            updateFoodCount(restaurantId, foodName, email, foodCount-1);
        }
    }

    public static void deleteItemRow(String restaurantId, String foodName, String email){
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "DELETE FROM USER_ORDER WHERE restaurantId = ? AND foodName = ? AND email = ? AND status= 'finding'";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantId);
            statement.setString(2, foodName);
            statement.setString(3, email);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void updateFoodCount(String restaurantId, String foodName, String email, int foodCount){
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "UPDATE USER_ORDER SET foodCount = ? WHERE email = ? AND restaurantId = ? AND foodName = ? AND status='finding'" ;
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setInt(1, foodCount);
            statement.setString(2, email);
            statement.setString(3, restaurantId);
            statement.setString(4, foodName);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insertItemRow(String restaurantName, String restaurantId, String foodName, String email, int orderId){
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "INSERT INTO USER_ORDER (restaurantName, restaurantId , foodName, email, foodCount, status, orderId) VALUES (?, ?, ?, ?, 1, 'finding', ?)";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, restaurantName);
            statement.setString(2, restaurantId);
            statement.setString(3, foodName);
            statement.setString(4, email);
            statement.setInt(5, orderId);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static int getFoodCountOrder(String foodName, String email){
        int count = 0;
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT foodCount FROM USER_ORDER WHERE email = ? AND foodName = ? AND status='finding'";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            statement.setString(2, foodName);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                count = result.getInt(1);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    public static void changeFindingStatusToFoundStatus(String email){
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "UPDATE USER_ORDER SET status = ? WHERE email = ? AND status = ?" ;
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, "found");
            statement.setString(2, email);
            statement.setString(3, "finding");
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void changeFoundStatusToDeliveredStatus(String email){
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "UPDATE USER_ORDER SET status = ? WHERE email = ? AND status='found'" ;
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, "delivered");
            statement.setString(2, email);
            statement.executeUpdate();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject getOrdersFromDBBaseOnStatus(String email, String status){
        String restaurantName = "";
        String restaurantId = "";
        String foodName = "";
        int foodCount = 0;
        int totalPrice = 0;
        JSONArray orderedFoodInfoArr = new JSONArray();
        JSONObject ordersInfo = new JSONObject();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT restaurantId, foodName, foodCount, restaurantName FROM USER_ORDER WHERE email = ? AND status=?";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            statement.setString(2, status);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                restaurantId = result.getString(1);
                foodName = result.getString(2);
                foodCount = result.getInt(3);
                restaurantName = result.getString(4);
                JSONObject orderedInfo = Cart.getOrderInfo(restaurantId,foodName,foodCount);
                orderedFoodInfoArr.add(orderedInfo);
                totalPrice += Cart.calculatePrice(orderedInfo);
            }
            ordersInfo.put("restaurantName", restaurantName);
            ordersInfo.put("order", orderedFoodInfoArr);
            ordersInfo.put("restaurantId", restaurantId);
            ordersInfo.put("totalPrice", totalPrice);
            ordersInfo.put("status", "finding");
            result.close();
            statement.close();
            con.close();
        } catch (SQLException | ParseException | InterruptedException e) {
            e.printStackTrace();
        }
        return ordersInfo;
    }

    public static JSONObject getDeliveredOrdersFromDBById(String email, int orderId) throws ParseException {
        String restaurantName = "";
        String restaurantId = "";
        String foodName = "";
        int foodCount = 0;
        int totalPrice = 0;
        JSONArray orderedFoodInfoArr = new JSONArray();
        JSONObject ordersInfo = new JSONObject();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT restaurantId, foodName, foodCount, restaurantName FROM USER_ORDER WHERE email = ? AND orderId = ? AND status='delivered'";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            statement.setInt(2, orderId);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                restaurantId = result.getString(1);
                foodName = result.getString(2);
                foodCount = result.getInt(3);
                restaurantName = result.getString(4);
                JSONObject orderedInfo = Cart.getOrderInfo(restaurantId,foodName,foodCount);
                orderedFoodInfoArr.add(orderedInfo);
                totalPrice += Cart.calculatePrice(orderedInfo);
            }
            ordersInfo.put("restaurantName", restaurantName);
            ordersInfo.put("order", orderedFoodInfoArr);
            ordersInfo.put("restaurantId", restaurantId);
            ordersInfo.put("totalPrice", totalPrice);
            ordersInfo.put("status", "delivered");
            result.close();
            statement.close();
            con.close();
        } catch (SQLException | ParseException | InterruptedException e) {
            e.printStackTrace();
        }
        return ordersInfo;
    }

    public static List<Integer> getDeliveredOrderId(String email){
        List<Integer> orderIdList = new ArrayList<>();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT DISTINCT orderId FROM USER_ORDER WHERE email = ? AND status='delivered'";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                int orderId = result.getInt(1);
                orderIdList.add(orderId);
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return orderIdList;
    }


    public static JSONArray getOlderOrder(String email){
        JSONArray array = new JSONArray();
        try {
            Connection con = ConnectionPool.getConnection();
            String sqlQuery = "SELECT DISTINCT orderId FROM USER_ORDER WHERE email = ? AND status='delivered'";
            PreparedStatement statement = con.prepareStatement(sqlQuery);
            statement.setString(1, email);
            ResultSet result = statement.executeQuery();
            while(result.next()){
                int orderId = result.getInt(1);
                array.add(getDeliveredOrdersFromDBById(email, orderId));
            }
            result.close();
            statement.close();
            con.close();
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
        return array;
    }
}
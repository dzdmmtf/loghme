package com.loghme.database;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public final class RestaurantMapper {
    private static final String TABLE_NAME = "Restaurant";

    public RestaurantMapper() {
        Statement statement;
        Connection conn;
        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();

            statement = conn.createStatement();
            statement.executeUpdate(String.format(
                    "create table if not exists %s (id varchar(255) primary key, name text, location text, logo text);", TABLE_NAME));
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void insert(JSONArray jsonArray) throws DataMapperException {
        Connection conn;
        PreparedStatement pStatement;
        String INSERT_USERS_SQL = "INSERT INTO Restaurant (id, name, location, logo) values (?, ?, ?, ?) " +
                                  "on duplicate key update name=values(name)";

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();

            pStatement = conn.prepareStatement(INSERT_USERS_SQL);
            conn.setAutoCommit(false);
            for (Object o : jsonArray) {
                JSONParser parser = new JSONParser();
                JSONObject jsonObject = (JSONObject) parser.parse(o.toString());
                pStatement.setString(1, (String) jsonObject.get("id"));
                pStatement.setString(2, (String) jsonObject.get("name"));
                pStatement.setString(3, jsonObject.get("location").toString());
                pStatement.setString(4, (String) jsonObject.get("logo"));
                pStatement.addBatch();
            }
            pStatement.executeBatch();
            conn.commit();
            conn.setAutoCommit(true);

            pStatement.close();
            conn.close();
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject find(String id) throws DataMapperException {
        Connection conn;
        JSONObject jsonObject = new JSONObject();

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();
            String sqlQuery = "SELECT * FROM Restaurant WHERE id = ?";
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                String restaurantId = result.getString("id");
                String name = result.getString("name");
                String location = result.getString("location");
                String logo = result.getString("logo");
                jsonObject.put("id", restaurantId);
                jsonObject.put("name", name);
                jsonObject.put("location", location);
                jsonObject.put("logo", logo);
            }
            result.close();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static JSONArray findPagination(Integer page, Integer count) throws DataMapperException {
        PreparedStatement pStatement = null;
        Connection conn = null;
        JSONArray jsonArray = new JSONArray();

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();

            pStatement = conn.prepareStatement("SELECT * FROM Restaurant limit ?,?");
            pStatement.setInt(1,(page-1)*count);
            pStatement.setInt(2, count);
            ResultSet result = pStatement.executeQuery();
            while (result.next()) {
                JSONObject jsonObject = new JSONObject();
                String id = result.getString("id");
                String name = result.getString("name");
                String location = result.getString("location");
                String logo = result.getString("logo");
                jsonObject.put("id", id);
                jsonObject.put("name", name);
                jsonObject.put("location", location);
                jsonObject.put("logo", logo);
                jsonArray.add(jsonObject);
            }
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            if (conn != null) try {
                assert pStatement != null;
                pStatement.close();
                conn.close();

            } catch (SQLException ignore) {}
        }
        return jsonArray;
    }

    public static JSONObject findByName(String name) throws DataMapperException {
        Connection conn;
        JSONObject jsonObject = new JSONObject();

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();
            String sqlQuery = "SELECT * FROM Restaurant WHERE name = ?";
            PreparedStatement statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, name);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                String restaurantId = result.getString("id");
                String location = result.getString("location");
                String logo = result.getString("logo");
                jsonObject.put("id", restaurantId);
                jsonObject.put("name", name);
                jsonObject.put("location", location);
                jsonObject.put("logo", logo);
            }
            result.close();
            statement.close();
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static String getName(String id) throws DataMapperException {
        PreparedStatement statement = null;
        Connection conn = null;
        String name = null;

        try {
            ConnectionPool.setupDataSource();
            conn = ConnectionPool.getConnection();
            String sqlQuery = "SELECT * FROM Restaurant WHERE id = ?";
            statement = conn.prepareStatement(sqlQuery);
            statement.setString(1, id);
            ResultSet result = statement.executeQuery();
            if (result.next())
                name = result.getString("name");

            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }  finally {
            if (conn != null) try {
                assert statement != null;
                statement.close();
                conn.close();
            } catch (SQLException ignore) {}
        }
        return name;
    }
}

FROM maven:3.6.0-jdk-11 as maven_builder

LABEL maintainer="n.shakoei@gmail.com"

WORKDIR /app

#RUN apt-get update
#RUN apt-get install -y maven

COPY pom.xml .
COPY src ./src

RUN mvn clean package

#________________________________TOMCAT________________________________#
FROM tomcat:8-jdk11-openjdk

LABEL maintainer="n.shakoei@gmail.com"

#COPY --from=maven_builder /app/target/LoghmeServlet.war /usr/local/tomcat/webapps
COPY --from=maven_builder /app/target/LoghmeServlet.war $CATALINA_HOME/webapps/LoghmeServlet.war

EXPOSE 8080


#docker build -t mywebapp .
# docker run -it --link mydbsql:mydatabase -p 8080:8080 --name=myloghmeapptest14 mywebapp

#something

CMD	["catalina.sh",	"run"]

